const {promises} = require('fs');
const {readdir, writeFile} = promises;

readdir('./public/assets/sponsors', {
  encoding: 'utf-8',
}).then(res => {
  const writeArray = Array.from(res)
    .map(sponsor => `'${sponsor}'`)
    .join(',');
  writeFile('./src/sponsors.ts', `export const SPONSORS = [${writeArray}]`, {
    encoding: 'utf-8',
  });
});
