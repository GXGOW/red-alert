export const ARTISTS: Array<{name: string; startTime: string}> = [
  {
    name: 'Mass Vibes',
    startTime: '21h',
  },
  {
    name: 'Cheaux',
    startTime: '22h30',
  },
  {
    name: 'THØR',
    startTime: '00h',
  },
  {
    name: 'Lester Williams',
    startTime: '01h30',
  },
  {
    name: 'No Trixx',
    startTime: '03h',
  },
];
