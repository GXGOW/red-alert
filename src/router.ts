import Vue from 'vue';
import Router from 'vue-router';

import Home from './components/Home.vue';
import Lineup from './components/Lineup.vue';
import Sponsors from './components/Sponsors.vue';
import Tickets from './components/Tickets.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
    },
    {
      path: '/lineup',
      name: 'lineup',
      component: Lineup,
    },
    {
      path: '/vvk',
      name: 'vvk',
      component: Tickets,
    },
    {
      path: '/sponsors',
      name: 'sponsors',
      component: Sponsors,
    },
    {
      path: '*',
      redirect: '/home',
    },
  ],
});
