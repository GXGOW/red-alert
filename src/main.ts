import 'firebase/analytics';
import 'firebase/storage';

import firebase from 'firebase/app';
import Vue from 'vue';

import FIREBASE_CONFIG from '../firebaseConfig.json';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router';

Vue.config.productionTip = false;

firebase.initializeApp(FIREBASE_CONFIG);
firebase.analytics();

new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app');
