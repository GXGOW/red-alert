# Red Alert

Vue.js project used for deployment at [https://www.red-alert.be].

## Run locally

You can run this locally by running following commands:

```bash
# Install dependencies
npm install
# Run Vue.js local webserver
npm run serve