module.exports = {
  publicPath: './',
  transpileDependencies: ['vuetify'],
  css: {
    loaderOptions: {
      sass: {
        implementation: require('sass'),
      },
    },
  }
};
