#### 2.0.0 (2020-05-14)

##### Chores

*  update dependencies (66a14ab7)
*  update dependencies (bf5f0010)
*  update dependencies (469b1c58)

##### New Features

*  move sponsor images to Firebase storage (f184443f)
*  add firebase configuration (7d52e77a)
* **footer:**  add version number (cf4a1419)

#### 1.3.1 (2020-02-09)

##### Chores

*  update dependencies (ca044124)

##### New Features

*  add event finished text (06e3596d)

##### Bug Fixes

* **countdown:**  timer finished styling (259702e7)

#### 1.3.0 (2020-02-04)

##### Chores

*  update tslint config (1a7b8cc4)
*  remove index.php (ca6d0759)

##### New Features

*  add route transition (fd5cc0ba)
* **lineup:**  add names + starttimes (9a6575c8)

##### Bug Fixes

* **sponsors:**  decrease slideshow speed (2d298c01)

#### 1.2.0 (2020-01-30)

##### Chores

*  update dependencies (a7092c15)

##### New Features

*  add sponsors page (64e8da9a)

##### Bug Fixes

*  faulty base url (821354eb)
*  faulty base url (cd2d49b8)
*  faulty base url (aed7c343)

#### 1.1.0 (2020-01-05)

##### Chores

*  update dependencies (9506e5e3)
*  update browserslist entries (1bc22b85)
*  update dependencies (d004016a)
*  update dependencies (314444a2)
*  update dependencies + configurations (49aae4fc)
*  update packages + year (431f1252)
*  cleanup gulp tasks (9565f487)
*  update packages + remove vue-analytics (9af64d96)

##### Continuous Integration

*  cache node_modules between stages (47a537f0)

##### New Features

*  add lineup page (e5360f07)
*  add tickets page (d73a712f)
*  update styling + add bottom nav (6d338827)
*  add header + footer (0e341c07)
*  add Vuetify (345aa5a4)
*  predefine end date as env variable + add date-fns for date computing (5e70026b)
* **countdown:**  glow on finish (150d1228)

##### Bug Fixes

*  invalid logo reference (c6d783c9)
*  proper link targeting (d1ea09f9)

##### Code Style Changes

*  vertical countdown on mobile (109fc637)

